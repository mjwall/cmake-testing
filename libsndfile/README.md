#### libsnfile

http://www.mega-nerd.com/libsndfile/#Download and https://github.com/erikd/libsndfile

This looks useful https://github.com/bbc/audiowaveform/blob/master/cmake/modules/FindLibSndFile.cmake

## Run

```
cmake -S . -B build
```

## Manual install

Just some notes

Cmake support is experimental.  The following steps failed for me

```
sudo yum install -y libogg-devel libvorbis-devel flac-devel alsa-lib-devel
curl -L https://github.com/erikd/libsndfile/archive/1.0.28.tar.gz -o ${HOME}/software/libsndfile-1.0.28.tar.gz
cd ~/software
tar xzf libsndfile-1.0.28.tar.gz 
cd libsndfile-1.0.28
cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/libsndfile-1.0.28 -DCMAKE_C_FLAGS="-fPIC" -B CMakeFiles -S .
cd CMakeFiles/
make
```

Getting compile errors,
```
[  0%] Building C object CMakeFiles/sndfile.dir/src/audio_detect.c.o
In file included from ~/opt/libsndfile-1.0.28-src/src/audio_detect.c:26:0:
/usr/include/unistd.h:1155:13: error: unknown type name ‘__n’
     ssize_t __n) __THROW __nonnull ((1, 2));
             ^
make[2]: *** [CMakeFiles/sndfile.dir/src/audio_detect.c.o] Error 1
make[1]: *** [CMakeFiles/sndfile.dir/all] Error 2
make: *** [all] Error 2
```

Master built fine with CMake.  Autotools worked fine with 1.0.28, so I am moving on for now

```
sudo yum install -y libogg-devel libvorbis-devel flac-devel alsa-lib-devel
curl -L https://github.com/erikd/libsndfile/archive/1.0.28.tar.gz -o ${HOME}/software/libsndfile-1.0.28.tar.gz
cd ~/software
tar xzf ~/software/libsndfile-1.0.28.tar.gz
cd libsndfile-1.0.28
./configure --prefix=${HOME}/opt/libsndfile-1.0.28
make
make install
```
