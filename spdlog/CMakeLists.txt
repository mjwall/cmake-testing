cmake_minimum_required(VERSION 3.16)

project(spdlog-test VERSION 1.0.0 LANGUAGES CXX)

include(FetchContent)
FetchContent_Declare(
  spdlog
  GIT_REPOSITORY "https://github.com/gabime/spdlog"
  GIT_TAG v1.4.2
)
#FetchContent_GetProperties(spdlog)
#if(NOT spdlog_POPULATED)
#  FetchContent_Populate(spdlog)
#  message(STATUS "spdlog found: ${spdlog_POPULATED}, source dir: ${spdlog_SOURCE_DIR} and binary dir: ${spdlog_BINARY_DIR}")
#endif()
#find_package(spdlog CONFIG VERSION 1.4 REQUIRED HINTS ${spdlog_BINARY_DIR})
FetchContent_MakeAvailable(spdlog)

add_executable(main main.cpp )
spdlog_enable_warnings(main)
target_link_libraries(main PRIVATE spdlog::spdlog)

# Create logs directory
file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/logs")
