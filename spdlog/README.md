# SPDLog

A library for logging, see https://github.com/gabime/spdlog

This files does the simplest thing possible.  What that means, is the configure and generate step only dowloads and configures spdlog.  It doesn't actually make the library.  We could use the header only library, but since there is option to precompile, I wanted to take that.  So the first time you run the `make all` target, it is going to go into the _deps directory and build spdlog.  Kinda neat.

Leaving in some comments about not using FetchContent_MakeAvailable but you should ignore those.

One last note, was originally using 1.3.1, but 1.4.2 is the latest and does a better job of working with FetchContent.

The project, spdlog, is a nice example of how to play nicely and be flexible.


## Run

```
cmake -S . -B build
```

Debugging

```
cmake -S. -Bbuild --trace |& tee -a out
```

## Manual install of spdlog


```
cd ~/software
git clone https://github.com/gabime/spdlog.git
cd spdlog/
git checkout -b 1.3.1 refs/tags/v1.3.1
cmake -H. -B build -DCMAKE_INSTALL_PREFIX=${HOME}/opt/spdlog-1.3.1 -DCMAKE_BUILD_TYPE=Release -DSPDLOG_BUILD_BENCH=OFF
cmake --build build --target install
ls -l ~/opt/spdlog-1.3.1/include
```
