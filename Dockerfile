FROM centos:centos7

ARG VERSION=0.0.1
ARG GCC_VERSION=8.3.0
ARG CMAKE_VERSION=3.16.2

LABEL org.label-schema.name="cmake-testing" \
      org.label-schema.description="A docker container to test cmake concepts" \
      org.label-schema.url="https://gitlab.com/mjwall/cmake-testing" \
      org.label-schema.vcs-url="https://gitlab.com/mjwall/cmake-testing" \
      org.label-schema.version="${VERSION}" \
      org.label-schema.schema-version="1.0"

# docker build -t mjwall/cmake-testing:${VERSION} .
# docker push mjwall/cmake-testing:${VERSION} 
# docker run --rm -it -v $PWD:/code mjwall/cmake-testing:${VERSION} bash 
# NOTE or from windows, something like
# winpty docker run --rm -it -v d:\\Code\\playground\\cmake-testing\\extended:/code mjwall/cmake-testing:0.0.1 bash
#
# you can also create a user to match your local user inside the container
# First, you must know your user id, group id, user name and group name
# use the `id` command
# uid=1000(mikewall) gid=1000(mikewall) groups=1000(mikewall)
# 
# Now when you run the container, create a group and user, for example
# groupadd -g 1000 mikewall && useradd -u 1000 -g 1000 mikewall
# su - mikewall
# Don't forget to export the LD_LIBRARY_PATH below if you change user
# 
# Then run you commands, exit back to root if you need install packages or whatever
# When you exit the container, the generated build artifacts will be owned by you on the host

# this was useful
# https://github.com/docker-library/gcc/blob/master/8/Dockerfile

# set variable for when gcc is installed
ENV LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH

RUN yum update -y && \
    # install some packages and clean up
    yum groupinstall -y "Development Tools" && \
    yum install -y openssl-devel which bzip2 wget bind-utils && \
    yum clean all && \
    # get the compiler
    mkdir /software && \
    cd /software && \
    curl -L "http://www.netgull.com/gcc/releases/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.gz" -o gcc-${GCC_VERSION}.tar.gz && \
    tar xzf gcc-${GCC_VERSION}.tar.gz && \
    cd gcc-${GCC_VERSION} && \
    ./contrib/download_prerequisites && \
    cd .. && \
    mkdir -p gcc-build && \
    cd gcc-build && \
    ../gcc-${GCC_VERSION}/configure  --enable-languages=c,c++,fortran --disable-multilib && \
    make -j `grep -c ^processor /proc/cpuinfo` && \
    make install-strip && \
    rm -rf /software/gcc-* && \
    # install cmake, https://cmake.org/install/
    cd /software && \
    curl -L "https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.tar.gz" -o cmake-${CMAKE_VERSION}.tar.gz && \
    tar xzf cmake-${CMAKE_VERSION}.tar.gz && \
    cd cmake-${CMAKE_VERSION} && \
    ./bootstrap -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make && \
    make install && \
    rm -rf /software/cmake-*

