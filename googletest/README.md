# GoogleTest

https://github.com/google/googletest

Fetch GoogleTest and use it in a build.  Didn't build googletest separately.

## Run

```
cmake -E remove_directory build && cmake -S. -Bbuild
```

You can then run the tests with

```
cmake --build build --target all test
```