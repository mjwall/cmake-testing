# Min required

Testing if cmake fails when you define a min version and then try to use a policy in a later version

For example https://cmake.org/cmake/help/latest/policy/CMP0083.html was introduced in 3.14.

So try running with 3.14 which works find or 3.13, which fails with

```
CMake Error at ~/opt/cmake-3.16.0-Linux-x86_64/share/cmake-3.16/Modules/CheckPIESupported.cmake:69 (message):
  check_pie_supported: Policy CMP0083 is not set
Call Stack (most recent call first):
  CMakeLists.txt:5 (check_pie_supported)


-- Configuring incomplete, errors occurred!
See also "cmake-testing/min_required/build/CMakeFiles/CMakeOutput.log".
```

## Run 

```
cmake -S . -B build
```