# CMake Testing

Various exercises as I learn CMake.  Each directory is a separate project.  Simply change to that directory and look at the README.md.  Typically just run

```
cmake -S . -B build
```

NOTE: the -S on the cmake command was added in 3.13.  I am currently running with the latest, 3.16.

## Requirements

### C++ Compiler

I am using the latest gcc 8, 8.3.0, since CUDA 10.1 doesn't support gcc 9

```
cd <SOMEDIR>
git clone https://gcc.gnu.org/git/gcc.git
cd gcc
git checkout -b 8.3.0 refs/tags/gcc-8_3_0-release
cd ..
mkdir gcc-build && cd gcc-build/
sudo apt install libmpfr-dev libmpc-dev
$PWD/../gcc/configure --prefix=$HOME/opt/gcc-8.3.0 --enable-languages=c,c++,fortran,go --disable-multilib
make -j `cat /proc/cpuinfo | grep ^processor` && make install
```

Took an hour with 12 cores

Then add this to ~/.bashrc

```
export GCC_HOME=${HOME}/opt/gcc-8.3.0
export PATH=${GCC_HOME}/bin:$PATH
export CPATH=${GCC_HOME}/include:$CPATH
export LD_LIBRARY_PATH=${GCC_HOME}/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=${GCC_HOME}/lib64:$LIBRARY_PATH
export LD_RUN_PATH=${GCC_HOME}/lib64:$LD_RUN_PATH
export CC=${GCC_HOME}/bin/gcc
export CXX=${GCC_HOME}/bin/g++
export FC=${GCC_HOME}/bin/gfortan
```

### CMake

Running with the current latest cmake from https://cmake.org/download/, which is 3.16 currently.  Here is how I installed on linux

```
curl -L https://github.com/Kitware/CMake/releases/download/v3.16.0/cmake-3.16.0-Linux-x86_64.tar.gz -o ~/software/cmake-3.16.0-Linux-x86_64.tar.gz
cd ~/opt
tar xzf cmake-3.16.0-Linux-x86_64.tar.gz
export PATH="${HOME}/opt/cmake-3.16.0-Linux-x86_64/bin:${PATH}
```

Test it with

```
which cmake
```

Put that export in you ~/.bashrc

## Resources

1. [CMake docs](https://cmake.org/cmake/help/v3.16/)
2. [CMake tutorial](https://cmake.org/cmake/help/v3.16/guide/tutorial/index.html)
3. [Professional CMake book](https://crascit.com/professional-cmake/)
4. [Modern CMake site](https://cliutils.gitlab.io/modern-cmake/)
5. [CMake Release Notes](https://cmake.org/cmake/help/v3.16/release/index.html)
6. [CMake Examples](https://github.com/pr0g/cmake-examples)
7. [Time to do CMake right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
8. [Effective modern CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1)
9. [CMake history in AOSA book](http://www.aosabook.org/en/cmake.html)
10. [KitWare gitlab](https://gitlab.kitware.com/cmake/cmake)
11. [CGold hitchhikers guide to CMake](https://cgold.readthedocs.io/en/latest/index.html)
12. [Kitware wiki](https://gitlab.kitware.com/cmake/community/wikis/home)
13. [Module source](https://github.com/Kitware/CMake/tree/master/Modules)
14. [Good Video](https://www.youtube.com/watch?v=bsXLMQ6WgIk&feature=youtu.be)
15. [Another video](https://www.youtube.com/watch?v=eC9-iRN2b04&feature=youtu.be)
16. [Workshop](https://henryiii.github.io/cmake_workshop/)
17. [Inspired by #4](https://github.com/Bagira80/More-Modern-CMake)

## Exercises

### min-required

Testing the `cmake_minimum_required` command.  See the [README.md](min-required/README.md) for more.

### properties

A place to test properties, see [README.md](properties/README.md)

### variable-test

A place to test variables, see [README.md](variable-test/README.md)

### subdirectory

Testing some of the variables when adding subdirectories, [README.md](subdirectory/README.md)

### functions-and-macros

Defined a function and macro and shows how args are handled surprisingly in macros, [README.md](functions-and-macros/README.md)

### install-config

Testing some install stuff, specifically what it looks like when an install config is generated, [README.md](install-config/README.md)

### find-stuff

Testing some `find_*` commands.  [README.md](find-stuff/README.md)

### cli11

Get and then use the CLI11 library, [README.md](cli11/README.md)

### googletest

Get and then use GoogleTest, see [README.md](googletest/README.md)

TODO: add googlemock

### cxxtimer

Get and use the header only cxxtimer library - [README.md](cxxtimer/README.md) 

### crc

Get and use the header only crc library - [README.md](crc/README.md) 

### cnpy

The cnpy library see [README.md](cnpy/README.md) 

### protobuf

Use protobuf [README.md](protobuf/README.md) 

### libsndfile

A library for reading and writing wav files among others [README.md](libsndfile/README.md) 

### spdlog

A logging library [README.md](spdlog/README.md) 

### eigen

TODO: add info [README.md](eigen/README.md) 

### python3

TODO: add info [README.md](python3/README.md) 

### pybind11

TODO: add info [README.md](pybind11/README.md) 

### mkl

TODO: add info [README.md](mkl/README.md) 

### mkldnn

TODO: add info [README.md](mkldnn/README.md) 

### onnxruntime

TODO: add info [README.md](onnxruntime/README.md) 

### abseil

TODO: add info [README.md](abseil/README.md)

### cuda

TODO: add info [README.md](cuda/README.md)

### Template

TODO: use what I have learned and make a template that can be reused.

### Super Build

Testing a concept [README.md](superbuild/README.md)

## TODOs

To see all the TODOs, run this

```
git grep TODO
```

## Extra info

Last one is not working, just capturing what I tried

```
include(CMakePrintHelpers)
#cmake_print_variables(BUILDSYSTEM_TARGETS)
#cmake_print_properties(
#  TARGETS my_target
#  PROPERTIES BUILDSYSTEM_TARGETS
#)

# Use -LAH to see all varialbes
# cmake -LAH
# Use this to see a trace for just CMakeLists.txt files
# cmake -S. -Bbuild --trace-source=CMakeLists.txt
# Use this to see way more
# cmake -S. -Bbuild --trace |& tee -a out
# Add --trace-expand to expand variables to values and --debug-output to put cmake in debug mode

## Get all properties that cmake supports
execute_process(COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)
cmake_print_variables(CMAKE_PROPERTY_LIST)
cmake_print_properties(TARGETS main PROPERTIES COMPILE_OPTIONS)
```

See current defaults

```
cmake --system-information information.txt
```

More info when running the generated build

```
cmake --build build -- VERBOSE=1
```

## Package Manager research

- https://www.reddit.com/r/cpp/comments/8t0ufu/what_is_a_good_package_manager_for_c/

- http://pfultz2.com/blog/2017/10/27/universal-package-manager/

### Hunter

- https://cpp-pm-hunter.readthedocs.io/en/latest/index.html
- https://github.com/cpp-pm/hunter/issues

### Conan

- https://docs.conan.io/en/latest/using_packages.html
- https://www.youtube.com/watch?v=jKG6cETLN3M&feature=youtu.be

### vcpkg

- https://docs.microsoft.com/en-us/cpp/build/vcpkg?view=vs-2019

### build2

- https://build2.org/

### cget

- http://cget.readthedocs.io/
- https://github.com/pfultz2/cget


## Dockerfile

To create an environment for testing this stuff.  Hosted at https://hub.docker.com/r/mjwall/cmake-testing.  CentOS 7 currently, more work to come on that.
