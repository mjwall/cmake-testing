# define project here so the BINARY DIR is under ext
project(superbuild-external NONE)

message(STATUS "Running ${CMAKE_PROJECT_NAME}")
message("CMAKE_SOURCE_DIR=${CMAKE_SOURCE_DIR}")
message("CMAKE_BINARY_DIR=${CMAKE_BINARY_DIR}")
message("CMAKE_CURRENT_SOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}")
message("CMAKE_CURRENT_BINARY_DIR=${CMAKE_CURRENT_BINARY_DIR}")

include(ExternalProject)
set(EP_BASE "${CMAKE_SOURCE_DIR}/ext/build" CACHE PATH "Directory where the external projects are configured and built")
set_property(DIRECTORY PROPERTY EP_BASE ${EP_BASE})
message(STATUS "Setting EP_BASE: ${EP_BASE}")

set(LUAJIT_INSTALL_DIR ${CMAKE_SOURCE_DIR}/ext/installs/luajit-2.0.1)
ExternalProject_Add(project_luajit
  #PREFIX "${CMAKE_CURRENT_BINARY_DIR}/luajit-2.0.1"
  INSTALL_DIR "${LUAJIT_INSTALL_DIR}"
  URL http://luajit.org/download/LuaJIT-2.0.1.tar.gz
  CONFIGURE_COMMAND ""
  BUILD_IN_SOURCE 1
  BUILD_COMMAND make PREFIX="${LUAJIT_INSTALL_DIR}"
  INSTALL_COMMAND make install PREFIX="${LUAJIT_INSTALL_DIR}"
)

# now setup a target to install the project, could just run 'make project_luajit' as well
add_custom_target(install_luajit
  DEPENDS project_luajit
)

