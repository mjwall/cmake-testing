# Finds LuaJIT include file and library. This module sets the following
# variables:
#
#  LUAJIT_FOUND        - Flag if LuaJit was found
#  LUAJIT_INCLUDE_DIRS - LuaJIT include directory
#  LUAJIT_LIBRARIES    - LuaJIT library path
#
#-------------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

message(STATUS "In FindLuaJIT.cmake: ${LuaJIT_DIR}")

find_path(LUAJIT_INCLUDE_DIRS luajit.h NO_DEFAULT_PATH HINTS ${LuaJIT_DIR}/include/luajit-2.0/)
find_library(LUAJIT_LIBRARIES libluajit-5.1.a NO_DEFAULT_PATH HINTS ${LuaJIT_DIR}/lib/)

message(STATUS "LUAJIT_INCLUDE_DIRS ${LUAJIT_INCLUDE_DIRS}")
message(STATUS "LUAJIT_LIBRARIES ${LUAJIT_LIBRARIES}")

find_package_handle_standard_args(
    LuaJIT
    DEFAULT_MSG
    LUAJIT_LIBRARIES
    LUAJIT_INCLUDE_DIRS
)
