# Super build 

This project is a superbuild, where external, or thirdparty dependencies, are in built in a subdirectory that the main build can then reference.  It uses the more flexibile and mature [ExternlProject](https://cmake.org/cmake/help/v3.16/module/ExternalProject.html) which means those dependencies are not actually downloaded and built until [build](https://cmake.org/cmake/help/v3.16/manual/cmake.1.html#build-a-project) as opposed to configure/generate time like the newer module [FetchContent](https://cmake.org/cmake/help/v3.16/module/FetchContent.html).  Therefore, it will be a 2 phase cmake build.  The first phase is the SUPERBUILD ON, to download, configure, compile and install the dependencies.  The second phase is SUPERBUILD OFF, which will compile this project's code using the dependencies.

Not really a superbuild at this point, because it doesn't bring in the src project.  TODO: fix that.

## Workflow

TODO: document better how this works

First run the superbuild
```
cmake -S. -Bbuild
cmake --build build --target install_ext
pushd build/ext/luajit-2.0.1/src/project_luajit/
make all install # installed to /usr/local/share
```

Then run the build
```
cmake -DUSE_SUPERBUILD=OFF -S. -Bbuild
cmake --build build --target all test
```

TODO: add more info here once this approach makes sense

## Refactor

Most patterns I saw ran with superbuild off or on.  Is there a way to make off the default, check for some file presence or somehting in the ext directory and alert to user to rerun with command ABC and be prepared to wait some amount of time?