# Install config

Want to test install() with an EXPORT.  First let's make an install.  Since it is just testing, set the install directory inside the binary directory

## Run

Remove the build dir and run cmake configure and generate
```
cmake -E remove_directory build && cmake -S. -Bbuild
```

Run the make
```
cmake --build build
```

Run the binary, since there is not target for this
```
./build/main 
```

Run make install
```
cmake --build build --target install
```

Or you can build and install at the same time
```
cmake --build build --target all install
```

