#include <iostream>

#include "person.pb.h"

int main(int argc, char** argv) 
{
    Person message;
    message.set_name("I am Protobuf");
    std::cout << message.name() << std::endl;
    return 0;
}