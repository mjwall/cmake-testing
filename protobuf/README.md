# protobuf

The CMakeLists.txt file used FetchContent to go get protobuf and checkout the 3.11.1 tag.  The FetchContent_MakeAvailable expects a CMakeLists.txt file in the root directory, but protobuf puts it under the cmake directory.  It is a bug reported at https://gitlab.kitware.com/cmake/cmake/issues/19875.  In the meantime, the first execute_process under FetchContent_Populate runs the cmake configure and generate with the appropriate arguments.  The second execute_process builds protoc, which could be done later if you wanted to setup targets.  In this example, the generated person.pb.h file depends on the  build/_deps/protobuf-git-src/cmake/build/protoc-3.11.1.0 target.  Instead, I chose to go ahead and build now.

The FindProtobuf documentation at https://cmake.org/cmake/help/v3.16/module/FindProtobuf.html is out of date.  The generate_protobuf_cpp is legacy, and you must set protobuf_MODULE_COMPATIBLE to ON if you want to use it that way.  The new way is to call generate_protobuf.  You can see a comparison of the 2 ways at https://github.com/protocolbuffers/protobuf/blob/master/examples/CMakeLists.txt.  For even more details, read the cmake files under build/_deps/protobuf-git-src/cmake/build/lib64/cmake/protobuf/ which are generated from that first execute_process.


## Run

```
cmake -S . -B build
```

## Manual install of protobuf

Just some notes I had about a manual install of protobuf, which takes 30 min or more on a decent machine

```
cd ~/software
git clone https://github.com/protocolbuffers/protobuf.git
cd protobuf/
git checkout v3.11.1
git submodule update --init --recursive
./autogen.sh
./configure --prefix=${HOME}/opt/protobuf-v3.11.1
make
make check
make install
```

Now you need to add stuff to your path and .bashrc

```
export PATH=${HOME}/opt/protobuf-v3.11.1/bin:${PATH}
```
