#include <iostream>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

int main()
{
    std::cout << "Starting main" << std::endl;
    std::cout << "Current path is " << fs::current_path() << std::endl;
}
