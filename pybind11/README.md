# pybind11

Pybind 11 is an in source dependency.  Let's pin it a version so we get consistent builds.  At the top of the vista-sdk project, do this

```
git clone https://github.com/pybind/pybind11.git
git checkout v2.4.3
```



## Run

```
cmake -S . -B build
```