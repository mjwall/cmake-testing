# Onnxruntime

Fetch onnxruntime and build

This may help https://gitlab.kitware.com/cmake/cmake/issues/18831

## Build

How I built on a CentOS box. See https://github.com/microsoft/onnxruntime/blob/master/BUILD.md

With CentOS 7, the default python is 2.  Running `sudo yum udpate` appears to have installed the `python3` rpm.  That is recent, used to have to either use SCL or install from source.

I did need to run `sudo yum install python3-devel`

But, it means we need to figure out the library and include path.  So run this

```
$> python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())"
/usr/include/python2.7
$> python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())"
/usr/include/python3.6m

$> python2 -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))"
/usr/lib64
$> python3 -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))"
/usr/lib64
```

And now we build with that, after reading the build scripts enough to figure out how to pass that info

```
cd ~/software/
git clone https://github.com/microsoft/onnxruntime.git
cd onnxruntime/
git checkout v1.0.0
git submodule update --init --recursive
```

At this point, there is an issue with onnxruntime I reported at https://github.com/microsoft/onnxruntime/issues/2576
In a nutshell, we need to be able to pass multiple options with the --cmake_extra_defines flag but the parsing
is broken.  So for now, patch the source code by hand.

```
$> git di tools/ci_build/
diff --git a/tools/ci_build/build.py b/tools/ci_build/build.py
index b6b54fd..c724de8 100755
--- a/tools/ci_build/build.py
+++ b/tools/ci_build/build.py
@@ -407,7 +407,7 @@ def generate_build_tree(cmake_path, source_dir, build_dir, cuda_home, cudnn_home
     else:
         cmake_args += ["-Donnxruntime_PYBIND_EXPORT_OPSCHEMA=OFF"]

-    cmake_args += ["-D{}".format(define) for define in cmake_extra_defines]
+    cmake_args += ["-D{}".format(d) for define in cmake_extra_defines for d in define.split(" ")]

     if is_windows():
         cmake_args += cmake_extra_args

```

You could also do things like tweak the CMakeCache.txt file by hand or run the ./build.sh mulitple times changing the args but not clearing the CMakeCache.txt file to have have same effect

```
./build.sh --config RelWithDebInfo --build_shared_lib --parallel --use_mkldnn --cmake_extra_defines "PYTHON_LIBRARY=/usr/lib64/python3.6 PYTHON_INCLUDE_DIR=/usr/include/python3.6m CMAKE_INSTALL_PREFIX=${HOME}/opt/onnxruntime-1.0.0"
cd build/Linux/RelWithDebInfo
make
make install
```

Also need to copy of the patched mkl-dnn for using later

```
cp -r build/Linux/RelWithDebInfo/mkl-dnn/install ~/opt/mkl-dnn-1.0.2p
```

Here is a list of the dependencies and version onnxruntime 1.0.0 has in cmake/external

- cub 1.8.0
- date 2.4.1
- dml (unclear)
- DNNLibrary 647d4c3f
- eigen 3.3.90
- gemmlowp 42c53187
- googletest 1.8.0
- grpc 1.19.1
- jemalloc 4.1.1
- mimalloc 1.0
- mkldnn 1.0.2 plus patches
- ngraph 0.26.0 plus patches
- nsync 1.20.2
- numpy (whatever version you have)
- onnx 1.6.0
- onnx-tensorrt 6.0.1
- openvino 2019_R1.1
- protobuf 3.6.1
- pybind11 2.4.0
- re2 30cad267
- spdlog 1.4.0
- tvm 0.5.0
- wil e8c599bc
- zlib 50893291


## Run

```
cmake -S . -B build
```