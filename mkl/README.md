# MKL

MKL is the Math Kernel Library from Intel which provides fast math.  See https://software.intel.com/en-us/mkl for more info.

Need to make mkl.h available somehow.  Is FindBLAS enough?

## Install MKL

To install on CentOS or Redhat from https://software.intel.com/en-us/articles/installing-intel-free-libs-and-python-yum-repo

```
 sudo yum-config-manager --add-repo https://yum.repos.intel.com/mkl/setup/intel-mkl.repo
 sudo rpm --import https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB 
 sudo yum install intel-mkl-2019.4-070
 source /opt/intel/compilers_and_libraries_2019.4.243/linux/mkl/bin/mklvars.sh intel64
```

Put that source in your ~/.bashrc

NOTE: This puts mkl on paths before whatever is there, so consider adding custom compiler paths later in your bashrc if you need that.

If you are on  Ubuntu

TODO

If you are on Windows

TODO


TODO: test findBLAS and findLAPACK, is that enough

## Run

```
cmake -S . -B build
```