#include <iostream>
using namespace std;

#include <mkl.h>

int main() 
{
    // do something that uses MKL
    cout << "Hello, MKL! "  << endl;
    return 0;
}