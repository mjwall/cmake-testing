# Extended Example

This repo is an attempt at the following.  The dependencies directory contains an independent CMakeLists.txt file that is responsible for downloading, compiling and installing all external or thirdparty dependencies.  FetchContent is too immature to handle all the one off cases for dependencies, but ExternalProject is close.  Since ExternalProject only runs at build time, it seemed easier to just make it independent.  Putting the install directory outside the binary directory allows you to remove the binary directory without having to wait to reinstall.

Having the main project and dependencies independent also prevents all the cmake files from the dependencies being loaded.  Instead, the main project is able to use built in and custom made find_package modules to provide a more consistent loading of includes and binaries.

This is not a superbuild, I am not trying to be fancy with any of this.  My intent to be simple to understand, troubleshoot and tweak.

The main project takes inspiration from https://gitlab.com/CLIUtils/modern-cmake/tree/master/examples/extended-project described on https://cliutils.gitlab.io/modern-cmake/chapters/basics/structure.html but not using git submodules.  Also took reference from https://github.com/statismo/statismo/tree/master/superbuild

## Run

First build the dependencies

```
cd dependencies
cmake -S. -Bbuild
cmake --build build
cd ..
```

Then run the main code

```
cmake -S. -Bbuild
cmake --build build --target all test
./build/apps/main
./build/apps/main2 --help
```

or with extra debugging info

```
cmake -S. -Bbuild --trace
cmake --build build -- VERBOSE=1
./build/apps/main
```

Pybindings compile but are not working, should be something like
```
cd bindings
pip install .
python3 tests/test.py
```


If on ubuntu, you may need to use `-DCMAKE_LIBRARY_ARCHITECTURE=x86_64-linux-gnu` when running cmake configure so built in libraries are picked up correctly

## TODO

1. clean up the custom targets, can just add all project-* in the DEPENDS of one custom target
2. need findMKL script, need examples of how to include mkldnn and onnxruntime in LoadThirdParty.cmake

## TO TRY

1. Abstract out the install paths so they are not hard coded in both project and dependencies cmake files.
2. Allow for dependencies to be preinstalled on the system and pass a location to the build that simply verifies the version.
3. Figure out a clean way to make the main project depend on the the dependencies project so we can do things like notify the user if they haven't run the dependencies or run the dependencies automatically when needed