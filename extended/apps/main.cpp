
#include <iomanip>  // Includes ::std::hex
#include <iostream> // Includes ::std::cout

#include "extended/mycrc.h"
#include "extended/mycnpy.h"
#include "extended/mycxxtimer.h"
#include "extended/myspdlog.h"
#include "extended/myeigen.h"
#include "extended/mylibsndfile.h"
#include "extended/myprotobuf.h"

int main(int argc, char ** argv)
{
  // crc
  const char myString[] = { 'H', 'E', 'L', 'L', 'O', ' ', 'W', 'O', 'R', 'L', 'D' };
  std::uint32_t crc = calculateCrc(myString);
  std::cout << "My CRC: " <<  std::hex << crc << std::endl;
  
  if (checkCNPY()) {
    std::cout << "My CNPY: good" << std::endl;
  } else {
    std::cout << "My CNPY: not so good" << std::endl;
  }

  std::cout << "My CXXTIMER: " << runTimer() << std::endl;

  std::cout << "My SPDLOG: " << std::endl;
  logStuff();

  std::cout << "My Eigen: " << std::endl;
  doEigen();

  std::cout << "My LIBSNDFILE: " << std::endl;
  runSndFile();

  std::cout << "My PROTOBUF: " << getPersonName() << std::endl;

  return 0;
}