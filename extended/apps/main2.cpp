#include <iostream>
using namespace std;

#include <CLI/CLI.hpp>

int main(int argc, char** argv) 
{
    CLI::App app{"This is app2"};

    std::string filename = "default";
    app.add_option("-f,--file", filename, "A help string");

    CLI11_PARSE(app, argc, argv);
    return 0;
}
