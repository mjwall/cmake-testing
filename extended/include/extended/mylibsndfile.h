
#ifndef EXTENDED_MYLIBSNDFILE_H
#define EXTENDED_MYLIBSNDFILE_H

static void read_file (const char * fname);
static void create_file (const char * fname, int format);
void runSndFile();

#endif //EXTENDED_MYLIBSNDFILE_H