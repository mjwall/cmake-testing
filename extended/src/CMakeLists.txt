add_library(mycrc mycrc.cpp)
target_include_directories(mycrc PUBLIC
  $<BUILD_INTERFACE:${CRCPP_INCLUDE_DIRS}>
#  $<INSTALL_INTERFACE:include/mycrc>

)
add_library(mycnpy mycnpy.cpp ${CNPY_LIBRARIES} ${ZLIB_LIBRARIES} )
target_include_directories(mycnpy PUBLIC
  $<BUILD_INTERFACE:${CNPY_INCLUDE_DIRS}>
)

add_library(mycxxtimer mycxxtimer.cpp)
target_include_directories(mycxxtimer PUBLIC
  $<BUILD_INTERFACE:${CXXTIMER_INCLUDE_DIRS}>
)

add_library(myspdlog myspdlog.cpp)
target_link_libraries(myspdlog spdlog::spdlog)

add_library(myeigen myeigen.cpp)
target_link_libraries(myeigen Eigen3::Eigen)

add_library(sample1 sample1.cpp)

add_library(mylibsndfile mylibsndfile.cpp)
target_link_libraries(mylibsndfile SndFile::sndfile)

add_library(myprotobuf myprotobuf.cpp)
target_link_libraries(myprotobuf person)