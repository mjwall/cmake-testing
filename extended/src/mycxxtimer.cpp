#include <cxxtimer.hpp>
#include "extended/mycxxtimer.h" 

std::string runTimer() {
  cxxtimer::Timer timer;
  timer.start();
  std::string prefix = "I ran a timer for ";
  timer.stop();
  std::string nanos = std::to_string(timer.count<std::chrono::nanoseconds>());
  return prefix + nanos + " nanoseconds.";
}