#include"cnpy.h"
#include<complex>
#include<cstdlib>
//#include <iostream>

bool checkCNPY() {
  const int Nx = 128;
  const int Ny = 64;
  const int Nz = 32;

  //set random seed so that result is reproducible (for testing)
  srand(0);
  //create random data
  std::vector<std::complex<double>> data(Nx*Ny*Nz);
  for(int i = 0;i < Nx*Ny*Nz;i++) data[i] = std::complex<double>(rand(),rand());
  //save it to file
  cnpy::npy_save("arr1.npy",&data[0],{Nz,Ny,Nx},"w"); // overwrite existing file
  //load it into a new array
  cnpy::NpyArray arr = cnpy::npy_load("arr1.npy");
  std::complex<double>* loaded_data = arr.data<std::complex<double>>();
  
  //make sure the loaded data matches the saved data
  bool size_matched = arr.word_size == sizeof(std::complex<double>);
  bool shape_matched = arr.shape.size() == 3 && arr.shape[0] == Nz && arr.shape[1] == Ny && arr.shape[2] == Nx;
  bool data_matched = true;
  for(int i = 0; i < Nx*Ny*Nz;i++) {
    if (data[i] != loaded_data[i]) {
      data_matched = false;
    }
  }
  //std::cout << "Size matched: " << size_matched << std::endl;
  //std::cout << "Shape matched: " << shape_matched << std::endl;
  //std::cout << "Data matched: " << data_matched << std::endl;
  return size_matched && shape_matched && data_matched;
}