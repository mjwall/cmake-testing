#include "CRC.h"
#include <cstdint>

#include "extended/mycrc.h"

std::uint32_t calculateCrc(const char stringp[]) {
    std::uint32_t crc = CRC::Calculate(stringp, sizeof(stringp), CRC::CRC_32());
    return crc;
};

