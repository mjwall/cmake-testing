
# Use the provided GTestConfig.cmake
# but be sure to include(GoogleTest) from CMake to discover all the tests before you run then
# provides target libraries GTest::gtest and GTest::gmock
set(GTest_DIR ${EP_INSTALL}/googletest/lib/cmake/GTest)
find_package(GTest CONFIG REQUIRED)
include(GoogleTest)

add_executable(UnitTest sample1_unittest.cpp)

target_link_libraries(UnitTest
PUBLIC
	sample1
PRIVATE
	GTest::gtest_main
)

gtest_discover_tests(UnitTest)
