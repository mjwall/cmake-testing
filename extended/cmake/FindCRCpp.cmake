# Finds CRCpp include file, sets the following:
#
#  CRCPP_FOUND        - Flag if CRCpp was found
#  CRCPP_INCLUDE_DIRS - CRCpp include directory
#
#-------------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

find_path(CRCPP_INCLUDE_DIRS CRC.h NO_DEFAULT_PATH HINTS ${CRCPP_DIR}/include)

#message(STATUS "CRCPP_DIR ${CRCPP_DIR}")
#message(STATUS "CRCPP_INCLUDE_DIRS ${CRCPP_INCLUDE_DIRS}")

find_package_handle_standard_args(
        CRCpp
        DEFAULT_MSG
        CRCPP_INCLUDE_DIRS
)
