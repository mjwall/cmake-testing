# TODO: add some code to ensure EP_INSTALL is set

# CRCpp - 
# custom find file, just provides $CRCPP_INCLUDE_DIRS
set(CRCPP_DIR ${EP_INSTALL}/crcpp)
find_package(CRCpp REQUIRED)

# Zlib
# use building FindZLIB
# This exposed $ZLIB_LIBRARIES
find_package(ZLIB REQUIRED)
# The build in is IMPORTED but not global, so it is not visible unless included at the parent scope
# we are doing that here, but if we weren't we could move it up with the following
#set(ZLIB_LIBRARIES ${ZLIB_LIBRARIES} PARENT_SCOPE)

# CNPY
# custom find file, provides CNPY_INCLUDE_DIRS and CNPY_LIBRARIES
set(CNPY_DIR ${EP_INSTALL}/cnpy)
find_package(CNPY REQUIRED)

# cxxtimer
# custom find file, provide CXXTIMER_INCLUDE_DIRS
set(CXXTIMER_DIR ${EP_INSTALL}/cxxtimer)
find_package(CXXTIMER REQUIRED)

# cli11
# Use the installed CLI11Config.cmake
# provides CLI11::CLI11 library target
set(CLI11_DIR "${EP_INSTALL}/cli11/lib/cmake/CLI11")
find_package(CLI11  REQUIRED)

# spdlog
# Use the installed spdlogConfig.cmake file
# provides spdlog::splog library target
set(spdlog_DIR ${EP_INSTALL}/spdlog/lib/spdlog/cmake)
find_package(spdlog 1.4.2 CONFIG REQUIRED)

# eigen3
# Use the installed Eigen3Config.cmake
# provides Eigen3::Eigen library target
set(Eigen3_DIR ${EP_INSTALL}/eigen/share/eigen3/cmake)
find_package(Eigen3 3.3.7 CONFIG REQUIRED)

# googletest - see tests/CMakeLists.txt

# libsndfile
# Use the isntalled SndFileConfig.cmake
# provides SndFile::sndfile library target
set(SndFile_DIR ${EP_INSTALL}/libsndfile/lib/cmake/SndFile)
find_package(SndFile CONFIG REQUIRED)

# protobuf - see proto/CMakeLists.txt

# pybind11 - see bindings/CMakeLists.txt