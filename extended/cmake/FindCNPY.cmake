# Finds CNPY include file, sets the following:
#
#  CNPY_FOUND        - Flag if CNPY was found
#  CNPY_INCLUDE_DIRS - CNPY include directory
#  CNPY_LIBRARIES    - CNPY libraries
#
#-------------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

find_path(CNPY_INCLUDE_DIRS cnpy.h NO_DEFAULT_PATH HINTS ${CNPY_DIR}/include)
find_library(CNPY_LIBRARIES libcnpy.a NO_DEFAULT_PATH HINTS ${CNPY_DIR}/lib/)

#message(STATUS "CNPY_DIR ${CNPY_DIR}")
#message(STATUS "CNPY_INCLUDE_DIRS ${CNPY_INCLUDE_DIRS}")
#message(STATUS "CNPY_LIBRARIES ${CNPY_LIBRARIES}")

find_package_handle_standard_args(
        CNPY
        DEFAULT_MSG
        CNPY_INCLUDE_DIRS
        CNPY_LIBRARIES
)
