# Finds CXXTIMER include file, sets the following:
#
#  CXXTIMER_FOUND        - Flag if CXXTIMER was found
#  CXXTIMER_INCLUDE_DIRS - CXXTIMER include directory
#
#-------------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

find_path(CXXTIMER_INCLUDE_DIRS cxxtimer.hpp NO_DEFAULT_PATH HINTS ${CXXTIMER_DIR}/include)

#message(STATUS "CXXTIMER_DIR ${CXXTIMER_DIR}")
#message(STATUS "CXXTIMER_INCLUDE_DIRS ${CXXTIMER_INCLUDE_DIRS}")

find_package_handle_standard_args(
        CXXTIMER
        DEFAULT_MSG
        CXXTIMER_INCLUDE_DIRS
)
