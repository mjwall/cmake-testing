# CLI11

A library for command line parsing, see  https://github.com/CLIUtils/CLI11

## Build local

Here is how I built CLI11 outside of cmake

```
cd <SOMEWHERE>
git clone https://github.com/CLIUtils/CLI11.git
cd CLI11/
git checkout -b 1.8.0 refs/tags/v1.8.0
mkdir build
cd build/
cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/CLI11-1.8.0 ..
make
make install
ls ~/opt/CLI11-1.8.0/include/CLI/CLI.hpp
```

## FetchContent

Since cli11 uses cmake, the FetchContent works well.  It is built correctly and then exposes targets back to the project. Linked the provided `CLI11::CLI11` alias target to the executable appears to be all that is needed.

## Run

```
cmake -E remove_directory build && cmake -S. -Bbuild
cmake --build build
./build/main --help
```