# mkldnn

From https://intel.github.io/mkl-dnn/dev_guide_build.html

```
cd software
git clone https://github.com/intel/mkl-dnn.git
cd mkl-dnn/
git tag
git checkout -b 1.1.1 refs/tags/v1.1.1
mkdir build
cd build 
cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/mkl-dnn-1.1.1 ..
make -j 4
make install
```

NOTE: onnx runtime 1.0.0 is using a patched version 1.0.2 which is build below

## Run

```
cmake -S . -B build
```