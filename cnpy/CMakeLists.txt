cmake_minimum_required(VERSION 3.16)

project(cnpy VERSION 1.0.0 LANGUAGES CXX)

include(FetchContent)
# call it mycnpy since cnpy defines targets with the name cnpy
# works just fine, but this way is clearer
FetchContent_Declare(
  mycnpy
  GIT_REPOSITORY "https://github.com/rogersce/cnpy.git"
  GIT_TAG 4e8810b1
)
FetchContent_MakeAvailable(mycnpy)
# can we alias the imported targets
add_library(Extern::CNPY ALIAS cnpy)

add_executable(main main.cpp)
target_include_directories(main PRIVATE ${mycnpy_SOURCE_DIR})
target_link_libraries(main Extern::CNPY)
