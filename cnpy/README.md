# CNPY

A C++ library to read and write files from NumPy.  See https://github.com/rogersce/cnpy.

## Build local

This library does not make releases, so we again will checkout the latest commit to help with consistency

```
git clone https://github.com/rogersce/cnpy.git
cd cnpy/
git checkout 4e8810b1
mkdir build
cd build/
cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/cnpy-4e8810b1 ..
make
make install
```

## FetchContent

The cnpy library uses cmake, but it doesn't work as well as cli11.  So you need to add the header and link the library.

Interesting to note that targets from cnpy are run.  Maybe that make sense cause it needs to be built

## Run

```
cmake -E remove_directory build && cmake -S. -Bbuild
cmake --build build
./build/main 
```