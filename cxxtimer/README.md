# cxxtimer

A timer class for C++, see https://github.com/andremaravilha/cxxtimer

## Build local

```
cd ~/software
git clone https://github.com/andremaravilha/cxxtimer.git
cd cxxtimer/
git checkout -b 1.0.0 refs/tags/v1.0.0
mkdir ~/opt/cxxtimer-1.0.0
cp cxxtimer.hpp ~/opt/cxxtimer-1.0.0/
```

## FetchContent

Doesn't use cmake but is just a header, so once FetchContent is called, a <projectName>_SOURCE_DIR is available to add to the target include

## Run

```
cmake -E remove_directory build && cmake -S. -Bbuild
cmake --build build 
./build/main 
```