#include "CRC.h" // Only need to include this header file!
                 // No libraries need to be included. No project settings need to be messed with.

#include <iomanip>  // Includes ::std::hex
#include <iostream> // Includes ::std::cout
#include <cstdint>  // Includes ::std::uint32_t

int main(int argc, char ** argv)
{
  const char myString[] = { 'H', 'E', 'L', 'L', 'O', ' ', 'W', 'O', 'R', 'L', 'D' };
  
  std::uint32_t crc = CRC::Calculate(myString, sizeof(myString), CRC::CRC_32());
  
  std::cout << std::hex << crc << std::endl;
  
  return 0;
}