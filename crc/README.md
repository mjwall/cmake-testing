# CRC

CRC library - see https://github.com/d-bahr/CRCpp

## Build local

Releases are not apparent in this repo.  Ticket #3 included a change to v1.0.0.0 but no tag was made a bug fix went in after that, see https://github.com/d-bahr/CRCpp/commit/7421f3256e772b549357143543b464d9194af2ab.

So we are going to check at the latest commit as of today (9 Dec 2019) and pin the version to that to help unsure consistency.

```
cd ~/software
git clone https://github.com/d-bahr/CRCpp.git
cd CPCpp
git checkout 534c1d8c
mkdir ~/opt/crcpp-534c1d8c
cp inc/CRC.h ~/opt/crcpp-534c18c/
```

## FetchContent

Similar to cxxtimer, but this project doesn't have releases so just put in a git hash.  The project hasn't been updated in 4 years, but using the hash will make builds consistent if it does get updated.

## Run

```
cmake -E remove_directory build && cmake -S. -Bbuild
cmake --build build
./build/main 
```