# python 3

Test the findPython3 module included in CMake, https://cmake.org/cmake/help/v3.16/module/FindPython3.html

## Run

```
cmake -S . -B build
```