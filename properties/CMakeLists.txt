cmake_minimum_required(VERSION 3.16)

project(prop-test)

get_cmake_property(allCommands COMMANDS)

get_cmake_property(allComponents COMPONENTS)

message(STATUS "Here the commands ${allCommands}")

message(STATUS "Here the components ${allComponents}")