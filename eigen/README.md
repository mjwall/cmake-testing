#### eigen

Using eigen 3, see 
http://eigen.tuxfamily.org/index.php?title=Main_Page and https://github.com/eigenteam/eigen-git-mirror

```
curl -L http://bitbucket.org/eigen/eigen/get/3.3.7.tar.gz -o ~/software/eigen-3.3.7.tar.gz                                             
cd ~/software
tar xzf ~/software/eigen-3.3.7.tar.gz
cd eigen-eigen-323c052e1731/
mkdir build
cd build/
cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/eigen-3.3.7 ..
make
make install
```

Maybe some tips at https://github.com/ceres-solver/ceres-solver/issues/451


## Run

```
cmake -S . -B build
```