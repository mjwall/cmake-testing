cmake_minimum_required(VERSION 3.16)

project(eigen-testing VERSION 1.0.0 LANGUAGES CXX)

include(FetchContent)

FetchContent_Declare(
  eigen-git
  GIT_URL
)

