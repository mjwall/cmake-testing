FROM ubuntu:18.04

# docker build -f ubuntu18-clang8.Dockerfile -t mjwall/cmake-u18-cl8 .

RUN apt-get update -y && apt-get install -y \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        emacs-nox \
        git \
        manpages-dev \
        nasm \
        screen \
        software-properties-common \
        sox \
        tmux \
        vim-tiny \
        wget \
        yasm \
        zlib1g-dev

RUN  cd /tmp && \
        wget https://apt.llvm.org/llvm-snapshot.gpg.key && \
        apt-key add llvm-snapshot.gpg.key && \
        rm llvm-snapshot.gpg.key && \
        add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-8 main" && \
        apt-get update -y && \
        apt-get install -y \
            python3-dev \
            python3-pip \
            clang-8 

RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.16.4/cmake-3.16.4-Linux-x86_64.tar.gz -o /tmp/cmake-3.16.4.tar.gz && \
        tar -C /opt -xzf /tmp/cmake-3.16.4.tar.gz && \
        rm /tmp/cmake-3.16.4.tar.gz

RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-8 80 --slave /usr/bin/clang++ clang++ /usr/bin/clang++-8 && \
        update-alternatives --install /usr/bin/cc cc /usr/bin/clang-8 80 && \
        update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-8 80

RUN echo "unset BASH_ENV PROMPT_COMMAND ENV\n\
export CC=/usr/bin/clang-8\n\
export CXX=/usr/bin/clang++-8\n\
export PATH=/opt/cmake-3.16.4-Linux-x86_64/bin:\${PATH}\n" >> /usr/bin/my-entry && \
    echo "#!/bin/bash\nsource /usr/bin/my-entry\nexec \"\$@\"" >> /usr/bin/entrypoint.sh && \
    chmod 755 /usr/bin/entrypoint.sh

ENV BASH_ENV="/usr/bin/my-entry" \
    ENV="/usr/bin/my-entry" \
    PROMPT_COMMAND=". /usr/bin/my-entry"

ENTRYPOINT [ "/usr/bin/entrypoint.sh" ]
CMD bash
